# Description
Transforms accented letters into uppercased accented letters when Caps Lock
key is toggled.  
CapsLock + é ⇒ É

# Use case
Some keyboards have keys with accented letters that are very useful when
writing in French for example. But when combining these keys with Caps Lock
key, the result is a bit dumb. Indeed, when you type _liberté_ (with Caps Lock
key	toggled and the special accented key _**é**_) what it writes is _LIBERT_**é** but
you	would rather write _LIBERT_**É**.  

**This is when capsLockAccent function is useful! :smile:**  

It transforms accented letters into uppercased accented letters when Caps Lock
is toggled. So you don't have to worry anymore when typing uppercased long
sentences using Caps Lock key.  
Moreover, it works also with cedilla (ç → Ç for French people) and n with a
tilde (ñ → Ñ for Spanish people) and can easily be adapted for other accented
keys.

# Supported keys
    a : àáâäã   →   ÀÁÂÄÃ
    c : ç       →   Ç
    e : èéêë    →   ÈÉÊË
    i : ìíîï    →   ÌÍÎÏ
    n : ñ       →   Ñ
    o : òóôöõ   →   ÒÓÔÖÕ
    u : ùúûü    →   ÙÚÛÜ
    y : ý       →   Ý

| Key pressed  | Output |
| :----------: | :----: |
| à            | À      |
| á            | Á      |
| â            | Â      |
| ä            | Ä      |
| ã            | Ã      |
| ç            | Ç      |
| è            | È      |
| é            | É      |
| ê            | Ê      |
| ë            | Ë      |
| ì            | Ì      |
| í            | Í      |
| î            | Î      |
| ï            | Ï      |
| ñ            | Ñ      |
| ò            | Ò      |
| ó            | Ó      |
| ô            | Ô      |
| ö            | Ö      |
| õ            | Õ      |
| ù            | Ù      |
| ú            | Ú      |
| û            | Û      |
| ü            | Ü      |
| ý            | Ý      |
