﻿/*
	==============================================================================
	Caps Lock Accent
	by D7ego
	More info at https://gitlab.com/smart-autohotkey/caps-lock-accent
	==============================================================================

	======= DESCRIPTION ==========================================================
	Transforms accented letters into uppercased accented letters when Caps Lock
	key is toggled.
	CapsLock + é ⇒ É

	– Supported keys
	a : àáâäã	→	ÀÁÂÄÃ
	c : ç		→	Ç
	e : èéêë	→	ÈÉÊË
	i : ìíîï	→	ÌÍÎÏ
	n : ñ		→	Ñ
	o : òóôöõ	→	ÒÓÔÖÕ
	u : ùúûü	→	ÙÚÛÜ
	y : ý		→	Ý


	======= USE CASE =============================================================
	Some keyboards have keys with accented letters that are very useful when
	writing in French for example. But when combining these keys with Caps Lock
	key, the result is a bit dumb. Indeed, when you type "liberté" (with Caps Lock
	key toggled and the special accented key "é") what it writes is "LIBERTé" but
	you	would rather write "LIBERTÉ".
	This is when capsLockAccent function is useful!
	It transforms accented letters into uppercased accented letters when Caps Lock
	is toggled. So you don't have to worry anymore when typing uppercased long
	sentences using Caps Lock key.
	Moreover, it works also with cedilla (ç → Ç for French people) and n with a
	tilde (ñ → Ñ for Spanish people) and can easily be adapted for other accented
	keys.
*/

capsLockAccent(){

	; If Caps Lock key is toggled, return the uppercase accented letter
	If(GetKeyState("Capslock", "T")){

		; Take the accented letter pressed
		StringRight, c, A_ThisHotkey, 1

		; Set it to uppercase
		StringUpper, c, c

		; Delete the last letter and insert the uppercased one
		SendInput {BS}%c%
	}
}

; List of all accented keys taking into account.
; If necessary, change it below to adapt to your keyboard.
#Hotstring * ? B0

	::à::
		capsLockAccent()
	Return

	::á::
		capsLockAccent()
	Return

	::â::
		capsLockAccent()
	Return

	::ä::
		capsLockAccent()
	Return

	::ã::
		capsLockAccent()
	Return

	::ç::
		capsLockAccent()
	Return

	::è::
		capsLockAccent()
	Return

	::é::
		capsLockAccent()
	Return

	::ê::
		capsLockAccent()
	Return

	::ë::
		capsLockAccent()
	Return

	::ì::
		capsLockAccent()
	Return

	::í::
		capsLockAccent()
	Return

	::î::
		capsLockAccent()
	Return

	::ï::
		capsLockAccent()
	Return

	::ñ::
		capsLockAccent()
	Return

	::ò::
		capsLockAccent()
	Return

	::ó::
		capsLockAccent()
	Return

	::ô::
		capsLockAccent()
	Return

	::ö::
		capsLockAccent()
	Return

	::õ::
		capsLockAccent()
	Return

	::ù::
		capsLockAccent()
	Return

	::ú::
		capsLockAccent()
	Return

	::û::
		capsLockAccent()
	Return

	::ü::
		capsLockAccent()
	Return

	::ý::
		capsLockAccent()
	Return

#Hotstring *0 ?0 B
